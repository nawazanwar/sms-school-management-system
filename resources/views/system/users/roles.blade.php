@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop


@section('content')
    <div class="card card-solid">
        <div class="card-header row align-items-center mx-1">
            <div class="card-title col-xl-6 col-lg-6 col-md-6">
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 text-right">
                <form action="{{ route('system.users.roles',[$model->id]) }}" method="get">
                    <div class="row justify-content-end">
                        <div class="col-xl-5">
                            <select class="custom-select custom-select-sm" name="field">
                                <option value="name" @if(isset($field) and $field == 'name') selected @endif>
                                    Name
                                </option>
                                <option value="label"
                                        @if(isset($field) and $field == 'label') selected @endif>
                                    Label
                                </option>
                            </select>
                        </div>
                        <div class="col-xl-5">
                            <input type="search" name="keyword"
                                   value="@if(isset($keyword) and $keyword != '') {{ $keyword }} @endif"
                                   class="form-control form-control-sm" placeholder="{{__('system.search_here')}}">
                        </div>
                        <div class="col-xl-2">
                            <input type="submit" class="btn btn-info btn-sm" value="{{ __('system.search') }}">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card-body pb-0">
            @include('partials.dashboard.message')
            <div class="form-group">
                <div class="row">
                    @php  $uModels = new \App\Models\User();  @endphp
                    @foreach($roles as $r)
                        @php $alreadyRole=$uModels->hasAlreadyRole($model->id,$r->id);@endphp
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 my-2">
                            <div class="icheck-success d-inline">
                                <input type="checkbox"
                                       onclick="assignRoleUser(this,'{{$model->id}}','{{$r->id}}')"
                                       {{ $alreadyRole==true?'checked':'' }} id="checkboxSuccess{{$r->id}}">
                                <label for="checkboxSuccess{{$r->id}}" class="font-weight-light">
                                    {{ $r->label }}<br>
                                </label>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
@endsection
@section('pageScript')
    <script type="text/javascript">
        function assignRoleUser (cElement, uId, rId) {
            var type, message;
            if ($(cElement).is(':checked')) {
                type = 'active';
            } else {
                type = 'deActive';
            }
            Ajax.call("/system/users/roles/assign/" + uId + "/" + rId + "/" + type, null, 'post', function (response) {
                if (response.type == 'active') {
                    message = "Roles of <strong>" + response.label + "</strong> assigned Successfully";
                    toastr.success(message, 'Success');
                } else {
                    message = "Roles of <strong>" + response.label + "</strong> removed Successfully";
                    toastr.error(message, 'Error');
                }
            })
        }
    </script>
@endsection
