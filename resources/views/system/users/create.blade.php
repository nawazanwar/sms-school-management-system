@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="card card-solid">
        @can('read',\App\Models\User::class)
            <div class="card-header">
                <div class="card-title">
                    {!! link_to_route('system.users',__('system.all_users'),null,['class'=>'btn btn-info btn-sm']) !!}
                </div>
            </div>
        @endcan
        <div class="card-body pb-0">
            <!-- form start -->
            {!! Form::open(['route' => ['system.users.store'], 'method' => 'POST']) !!}
            {!! csrf_field() !!}
            @include('partials.dashboard.message')
            <div class="form-group">
                {!! Form::label('inputName', __('system.name')) !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'required', 'autofocus','placeholder'=>__('system.placeholder_name'), 'id' => 'inputName' ]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('inputEmail', __('system.email')) !!}
                {!! Form::text('email', null, ['class' => 'form-control', 'required', 'id' => 'inputEmail','placeholder'=>__('system.placeholder_email') ]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('inputTempPassword', __('system.temp_pwd')) !!}
                {!! Form::password('temp_password', ['class' => 'form-control', 'required', 'id' => 'inputTempPassword','placeholder'=>__('system.placeholder_temp_password') ]) !!}
            </div>
            <div class="form-group icheck-success d-inline">
                {!! Form::checkbox('active', 1, true,['id'=>'inputActive']) !!}
                {!! Form::label('inputActive', __('system.is_active')) !!}
            </div>

            <div class="form-group text-right">
                {!! Form::submit(__('system.save'), array('class' => 'btn btn-primary pull-left btn-sm')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
