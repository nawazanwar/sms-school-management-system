@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header row align-items-center mx-1">
                    <div class="card-title col-xl-6 col-lg-6 col-md-6">
                        @can('create',\App\Models\Role::class)
                            {!! link_to_route('system.roles.create',__('system.create'),null,['class'=>'btn bg-gradient-primary btn-sm']) !!}
                        @endcan
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 text-right">
                        <form action="{{ route('system.roles') }}" method="get">
                            <div class="row justify-content-end">
                                <div class="col-xl-5">
                                    <select class="custom-select custom-select-sm" name="field">
                                        <option value="name" @if(isset($field) and $field == 'name') selected @endif>
                                            Name
                                        </option>
                                        <option value="label"
                                                @if(isset($field) and $field == 'label') selected @endif>
                                            Label
                                        </option>
                                    </select>
                                </div>
                                <div class="col-xl-5">
                                    <input type="search" name="keyword"
                                           value="@if(isset($keyword) and $keyword != '') {{ $keyword }} @endif"
                                           class="form-control form-control-sm"
                                           placeholder="{{__('system.search_here')}}">
                                </div>
                                <div class="col-xl-2">
                                    <input type="submit" class="btn btn-info btn-sm" value="{{ __('system.search') }}">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-body">
                    @include('partials.dashboard.message')
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Label</th>
                            <th class="text-center">Created At</th>
                            <th class="text-center">Last Modified</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $d)
                            <tr>
                                <td>{{$d->name}}</td>
                                <td>{{$d->label}}</td>
                                <td class="text-center">{{$d->created_at}}</td>
                                <td class="text-center">{{$d->updated_at->diffForHumans()}}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default">Action</button>
                                        <button type="button" class="btn btn-default dropdown-toggle"
                                                data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            @can('edit',\App\Models\Role::class)
                                                <li class="dropdown-item">{!! link_to_route('system.roles.edit', __('system.edit'), [$d->id]) !!}</li>
                                            @endif
                                            @can('delete',\App\Models\Role::class)
                                                <li class="dropdown-item">{!! link_to_route('system.roles.delete', __('system.delete'), [$d->id]) !!}</li>
                                            @endcan
                                            @can('read',\App\Models\User::class)
                                                <li class="dropdown-item">{!! link_to_route('system.roles.users', __('system.all_users'), [$d->id]) !!}</li>
                                            @endcan
                                            @can('read',\App\Models\Permission::class)
                                                <li class="dropdown-item">{!! link_to_route('system.roles.permissions', __('system.permissions'), [$d->id]) !!}</li>
                                            @endcan
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix">
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>
@stop
