<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                @if(isset($pageTitle) && $pageTitle != '')
                    <h1 class="m-0 text-dark"> {{$pageTitle}} </h1>
                @endif
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href=" {{ route('home') }}">{{__('dashboard.home')}}</a></li>
                    @foreach($breadcrumbs as $leaf)
                        @if(!empty($leaf['url']))
                            <li class="breadcrumb-item"><a href="{{$leaf['url']}}">{{$leaf['text']}}</a></li>
                        @else
                            <li class="breadcrumb-item active">{{$leaf['text']}}</li>
                        @endif
                    @endforeach
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
