@php
    $cRouteName = \Request::route()->getName();
@endphp
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('home') }}" class="brand-link">
        <img src=" {{ asset('img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-bold">{{ __('dashboard.site_title') }}</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <a href="#" class="d-block">{{ Auth::user()->name }}</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-link active py-0 my-2">
                    <a href="{{ route('home') }}" class="nav-link active">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item has-treeview @if(strpos(Request::url(), 'system')) {{ 'menu-open' }} @endif">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-lock"></i>
                        <p>
                            System
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @can('read',\App\Models\Role::class)
                            <li class="nav-item ">
                                <a class="nav-link {{ ($cRouteName=='system.roles'?'active':'') }}"
                                   href="{{route('system.roles')}}">
                                    <i class="far fa-circle nav-icon {{ ($cRouteName=='system.roles'?'text-primary':'') }}"></i>
                                    All Roles
                                </a>
                            </li>
                        @endcan
                        @can('read',\App\Models\Permission::class)
                            <li class="nav-item ">
                                <a class="nav-link {{ ($cRouteName=='system.permissions'?'active':'') }}"
                                   href="{{route('system.permissions')}}">
                                    <i class="far fa-circle nav-icon {{ ($cRouteName=='system.permissions'?'text-primary':'') }}"></i>
                                    All Permissions
                                </a>
                            </li>
                        @endcan
                        @can('sync',\App\Models\Permission::class)
                            <li class="nav-item">
                                <a class="nav-link {{ ($cRouteName=='system.permissions.sync'?'active':'') }}"
                                   href="{{route('system.permissions.sync')}}">
                                    <i class="far fa-circle nav-icon {{ ($cRouteName=='system.permissions.sync'?'text-primary':'') }}"></i>
                                    Sync Permissions
                                </a>
                            </li>
                        @endcan
                        @can('read',\App\Models\User::class)
                            <li class="nav-item ">
                                <a class="nav-link {{ ($cRouteName=='system.users'?'active':'') }}"
                                   href="{{route('system.users')}}">
                                    <i class="far fa-circle nav-icon {{ ($cRouteName=='system.users'?'text-primary':'') }}"></i>
                                    All Users
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
