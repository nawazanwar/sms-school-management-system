<footer class="main-footer py-2 fixed-bottom">
    <p class="text-center mb-0">Copyright © {{ date('Y') }} All rights reserved.</p>
</footer>
