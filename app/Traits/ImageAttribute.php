<?php

namespace App\Traits;

trait ImageAttribute
{

    public function getImageAttribute($value)
    {

        if ($value) {

            $s3 = \Storage::disk('s3');

            $client = $s3->getDriver()->getAdapter()->getClient();

            $expiry = "+10 minutes";

            $command = $client->getCommand('GetObject', [

                'Bucket' => \Config::get('filesystems.disks.s3.bucket'),

                'Key' => $this->imageDirectory . '/' . $value,

            ]);

            $request = $client->createPresignedRequest($command, $expiry);

            return (string)$request->getUri();

        }

        return $this->defaultImage;
    }

}
