<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model implements Permissions
{
    use SoftDeletes;
    protected $fillable = ['name', 'label'];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function hasPermission($role_id, $permission_id)
    {
        return DB::table('permission_role')->where(['role_id' => $role_id, 'permission_id' => $permission_id])->exists();
    }

    public function hasUser($role_id, $user_id)
    {
        return DB::table('role_user')->where(['role_id' => $role_id, 'user_id' => $user_id])->exists();
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_role');
                    break;
                case 'create':
                case 'store':
                    return array('create_role');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_role');
                    break;
                case 'delete':
                    return array('delete_role');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_role',
            'create_role',
            'edit_role',
            'delete_role',
        );
    }
}
