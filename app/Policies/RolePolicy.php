<?php

namespace App\Policies;

use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class RolePolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
      return $user->ability('read_role');
    }

    public function create(User $user)
    {
        return $user->ability('create_role');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_role');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_role');
    }
}
