<?php

namespace App\Http\Controllers\System;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Permission;

class PermissionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = new Permission();
        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        if ($field && $keyword) {
            $data = $data->where('permissions.' . $field, 'like', '%' . $keyword . '%');
        }
        $data = $data->orderby('permissions.id', 'DESC')->paginate(10);
        $pageTitle = __('system.all_permissions');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'field' => $field,
            'keyword' => $keyword,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $data,
        ];
        return view('system.permissions.index', $viewParams);
    }

    public function create()
    {

        $pageTitle = __('system.create_permission');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
        ];

        return view('system.permissions.create', $viewParams);
    }

    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'name' => 'required|max:255|unique:permissions,name',
            'label' => 'required|max:255',
        ]);
        if (Permission::create($validatedData)) {
            return redirect()->route('system.permissions')->with('successMessage', __('system.permission_created_success_message'));
        } else {
            return redirect()->back()
                ->withErrors(__('system.permission_created_failed_message'))
                ->withInput();
        }
    }

    public function edit($id)
    {
        $model = Permission::find($id);
        $pageTitle = __('system.edit') . " " . $model->label;
        $breadcrumbs = [['text' => __('system.edit_permission')]];
        $viewParams = [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle
        ];
        return view('system.permissions.edit', $viewParams);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:4|unique:permissions,name,' . $id,
            'label' => 'required|max:255',
        ]);

        if (Permission::whereId($id)->update($validatedData)) {
            return redirect()->route('system.permissions')->with('successMessage', $validatedData['label'] . " " . __('system.updated_success_message'));
        } else {
            return redirect()->back()
                ->withErrors($validatedData['label'] . " " . __('system.updated_failed_message'))
                ->withInput();
        }
    }

    public function destroy($id)
    {
        $model = Permission::find($id);
        if ($model->delete()) {
            return redirect()->route('system.permissions')->with('errorMessage', $model->label . " " . __('system.deleted_successfully'));
        }
    }

    public function sync()
    {
        if (Permission::syncPermissions()) {
            Session::flash('successMessage', __('system.permission_sync'));
            return redirect()->back();
        } else {
            return redirect()->back()->withErrors(__('system.failed_permission_sync'));
        }
    }
}
