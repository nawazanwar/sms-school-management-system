<?php

namespace App\Http\Controllers\System;

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = new User();
        $data = $data->withTrashed();
        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        if ($field && $keyword) {
            $data = $data->where('users.' . $field, 'like', '%' . $keyword . '%');
        }
        $data = $data->orderby('users.id', 'DESC')->paginate(10);
        $pageTitle = __('system.all_users');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'field' => $field,
            'keyword' => $keyword,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $data,
        ];
        return view('system.users.index', $viewParams);
    }

    public function create()
    {

        $pageTitle = __('system.create_user');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
        ];

        return view('system.users.create', $viewParams);
    }

    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'email' => 'required|max:255|unique:users,email',
            'name' => 'required|max:255',
            'temp_password' => 'required|max:255',
            'active' => 'bool|required'
        ]);
        $validatedData['password'] = Hash::make($request->input('temp_password'));
        if (User::create($validatedData)) {
            return redirect()->route('system.users')->with('successMessage', __('system.user_created_success_message'));
        } else {
            return redirect()->back()
                ->withErrors(__('system.user_created_failed_message'))
                ->withInput();
        }
    }

    public function edit($id)
    {
        $model = User::find($id);
        $pageTitle = __('system.edit') . " " . $model->label;
        $breadcrumbs = [['text' => __('system.edit_user')]];
        $viewParams = [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle
        ];

        return view('system.users.edit', $viewParams);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:4|unique:roles,name,' . $id,
            'label' => 'required|max:255',
        ]);

        if (Role::whereId($id)->update($validatedData)) {
            return redirect()->route('system.roles')->with('successMessage', $validatedData['label'] . " " . __('system.updated_success_message'));
        } else {
            return redirect()->back()
                ->withErrors($validatedData['label'] . " " . __('system.updated_failed_message_message'))
                ->withInput();
        }
    }

    public function destroy($id)
    {
        $model = User::find($id);
        if ($model->delete()) {
            return redirect()->route('system.users')->with('errorMessage', $model->name . " " . __('system.deleted_successfully'));
        }
    }

    public function roles($id, Request $request)
    {
        $model = User::find($id);
        $roles = Role::all();
        $pageTitle = __('system.all_roles_of') . " " . $model->name;
        $breadcrumbs = [['text' => __('system.all_permissions')]];
        $viewParams = [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'roles' => $roles
        ];
        return view('system.users.roles', $viewParams);
    }

    public function assignRole($uId, $rId, $type)
    {
        $model = User::find($uId);
        $role = Role::find($rId);
        if ($type == 'deActive') {
            $model->roles()->detach($rId);
        } else {
            $model->roles()->save($role);
        }
        return response()->json(['type' => $type, 'label' => $role->label]);
    }
}
