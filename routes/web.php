<?php

use App\Models\Role;
use App\Models\Permission;
use App\Models\User;

Auth::routes();


Route::group(['prefix' => '/', 'middleware' => ['permissions']], function () {

    Route::get('', ['as' => 'home', 'uses' => 'DashboardController@index']);

    //Roles

    Route::get('system/roles', ['as' => 'system.roles', 'uses' => 'System\RoleController@index',
        'permissions' => Role::modulePermissions(true, 'read')]);
    Route::get('system/roles/create', ['as' => 'system.roles.create', 'uses' => 'System\RoleController@create',
        'permissions' => Role::modulePermissions(true, 'create')]);
    Route::post('system/roles/store', ['as' => 'system.roles.store', 'uses' => 'System\RoleController@store',
        'permissions' => Role::modulePermissions(true, 'store')]);
    Route::get('system/roles/edit/{id}', ['as' => 'system.roles.edit', 'uses' => 'System\RoleController@edit',
        'permissions' => Role::modulePermissions(true, 'edit')]);
    Route::put('system/roles/update/{id}', ['as' => 'system.roles.update', 'uses' => 'System\RoleController@update',
        'permissions' => Role::modulePermissions(true, 'update')]);
    Route::get('system/roles/delete/{id}', ['as' => 'system.roles.delete', 'uses' => 'System\RoleController@destroy',
        'permissions' => Role::modulePermissions(true, 'delete')]);
    Route::get('system/roles/permissions/{id}', ['as' => 'system.roles.permissions', 'uses' => 'System\RoleController@permissions',
        'permissions' => Permission::modulePermissions(true, 'read')]);
    Route::post('system/roles/permissions/assign/{rId}/{pId}/{type}', ['as' => 'system.roles.permissions.assign', 'uses' => 'System\RoleController@assignPermission']);
    Route::get('system/roles/users/{id}', ['as' => 'system.roles.users', 'uses' => 'System\RoleController@users',
        'permissions' => User::modulePermissions(true, 'read')]);
    Route::post('system/roles/users/assign/{rId}/{uId}/{type}', ['as' => 'system.roles.users.assign', 'uses' => 'System\RoleController@assignUser']);

    // permissions

    Route::get('system/permissions', ['as' => 'system.permissions', 'uses' => 'System\PermissionController@index',
        'permissions' => Permission::modulePermissions(true, 'read')]);
    Route::get('system/permissions/sync', ['as' => 'system.permissions.sync', 'uses' => 'System\PermissionController@sync',
        'permissions' => Permission::modulePermissions(true, 'sync')]);
    Route::get('system/permissions/edit/{id}', ['as' => 'system.permissions.edit', 'uses' => 'System\PermissionController@edit',
        'permissions' => Permission::modulePermissions(true, 'edit')]);
    Route::put('system/permissions/update/{id}', ['as' => 'system.permissions.update', 'uses' => 'System\PermissionController@update',
        'permissions' => Permission::modulePermissions(true, 'update')]);
    Route::get('system/permissions/create', ['as' => 'system.permissions.create', 'uses' => 'System\PermissionController@create',
        'permissions' => Permission::modulePermissions(true, 'create')]);
    Route::post('system/permissions/store', ['as' => 'system.permissions.store', 'uses' => 'System\PermissionController@store',
        'permissions' => Permission::modulePermissions(true, 'store')]);
    Route::get('system/permissions/delete/{id}', ['as' => 'system.permissions.delete', 'uses' => 'System\PermissionController@destroy',
        'permissions' => Permission::modulePermissions(true, 'delete')]);

    // Users

    Route::get('system/users', ['as' => 'system.users', 'uses' => 'System\UserController@index',
        'permissions' => User::modulePermissions(true, 'read')]);
    Route::get('system/users/edit/{id}', ['as' => 'system.users.edit', 'uses' => 'System\UserController@edit',
        'permissions' => User::modulePermissions(true, 'edit')]);
    Route::put('system/users/update/{id}', ['as' => 'system.users.update', 'uses' => 'System\UserController@update',
        'permissions' => User::modulePermissions(true, 'update')]);
    Route::get('system/users/create', ['as' => 'system.users.create', 'uses' => 'System\UserController@create',
        'permissions' => User::modulePermissions(true, 'create')]);
    Route::post('system/users/store', ['as' => 'system.users.store', 'uses' => 'System\UserController@store',
        'permissions' => User::modulePermissions(true, 'store')]);
    Route::get('system/users/delete/{id}', ['as' => 'system.users.delete', 'uses' => 'System\UserController@destroy',
        'permissions' => User::modulePermissions(true, 'delete')]);
    Route::get('system/users/roles/{id}', ['as' => 'system.users.roles', 'uses' => 'System\UserController@roles',
        'permissions' => Role::modulePermissions(true, 'read')]);
    Route::post('system/users/roles/assign/{uId}/{rId}/{type}', ['as' => 'system.users.roles.assign', 'uses' => 'System\UserController@assignRole']);

});

Route::get('/logout', ['as' => 'auth.logout', 'uses' => 'Auth\LoginController@logout']);

