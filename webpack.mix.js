const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .styles([
        'resources/theme/plugins/fontawesome-free/css/all.min.css',
        'resources/theme/css/adminlte.min.css',
        'resources/sass/admin.scss',
        'resources/sass/custom.scss'
    ], 'public/css/vendor.min.css')
    .scripts([
        'resources/theme/plugins/jquery/jquery.min.js',
        'resources/theme/plugins/bootstrap/js/bootstrap.bundle.min.js',
        'resources/theme/js/adminlte.min.js',
        'resources/js/custom.js'
    ], 'public/js/vendor.min.js')
    .copy([
        'resources/theme/plugins/fontawesome-free/webfonts',
    ], 'public/webfonts')
    .copy([
        'resources/theme/plugins/',
    ], 'public/plugins')
    .copy([
        'resources/theme/img',
    ], 'public/img');
